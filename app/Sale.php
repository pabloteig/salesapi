<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = ['receipt','sale_date','cashier','status','amount'];
    protected $guarded = ['id', 'created_at', 'update_at'];

}
