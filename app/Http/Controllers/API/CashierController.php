<?php

namespace App\Http\Controllers\API;

use App\Cashier;
use App\Http\Controllers\Controller;

class CashierController extends Controller
{

    public function index()
    {
        $cashiers = Cashier::all();

        if(strlen($cashiers) <= 2) {
            return response()->json('', 204);
        }

        return response()->json($cashiers);
    }
}
