<?php

namespace App\Http\Controllers\API;

use App\Sale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SaleController extends Controller
{

    public function index()
    {
        $sales = Sale::all();
        return response()->json($sales);
    }

    public function listSales(Request $request)
    {
        $data = $request->all();

        $matchThese = array();
        $dateStart = null;
        $dateEnd = null;

        if(array_key_exists("cashier",$data)) {
            $matchThese['cashier'] = $data["cashier"];
        }
        if(array_key_exists("status",$data)) {
            $matchThese['status'] = $data["status"];
        }
        if(array_key_exists("date_start",$data)) {
            $dateStart = $data["date_start"];
            if(array_key_exists("date_end",$data)) {
                $dateEnd = $data["date_end"];
            }else {
                $dateEnd = $dateStart;
            }
        }

        if(isset($dateStart)) {
            $sale = Sale::where($matchThese)
                ->whereBetween('sale_date', [$dateStart, $dateEnd])
                ->get();
        }else {
            $sale = Sale::where($matchThese)->get();
        }

        if(strlen($sale) <= 2) {
            return response()->json('', 204);
        }

        return response()->json($sale);
    }

    public function show($id)
    {
        $sale = Sale::where('receipt', $id)->first();

        if(!$sale) {
            return response()->json([
                'message'   => 'Sale not found',
            ], 404);
        }

        return response()->json($sale);
    }

}
