# SalesAPI

SalesAPI � parte de uma User Story, que tem como objetivo retornar as vendas realizadas de um comerciante.


# Ferramentas utilizadas e ambiente de desenvolvimento
	>Linux - ubuntu 16.04LTS
	>PhpStorm 2018.1.4
	
	>Composer V1.6.5
	>Laravel 5.6
	>PHP 7.1.17
	>Mysql 5.7.22
	
# Ferramentas utilizadas e ambiente de desenvolvimento
Siga os passos para executar o projeto (estes passos s�o para ambiente Linux, adapte para Windows).

	1- Clonar o projeto na pasta /var/www/html/ (utilizando Linux);
	2- Abra o terminal e execute o comando "composer install" para gerar a pasta vendor;
	3- Criar um schema na base de dados;
	4- Fazer uma c�pia do arquivo .env.example dexando apenas como .env (o arquivo encontra-se na raiz do projeto) e configurar de acordo com as informa��es do banco de dados;
	5- Abra o terminal e execute o seguinte comando "php artisan migrate" para criar as tabelas de acordo com as migrations criadas;
	6- Ainda no terminal, execute o seguinte comando "php artisan db:seed" para popular a base de acordo com as Seedres criadas. Pode executar este comando "php artisan migrate:refresh --seed" para limpar o banco e popular novamente a base de dados;
	7- Para garantir que os passos anteriores funcionaram, pode verificar o banco de dados a fim de verificar se gerou volume de dados, caso tenha gerado, poder� testar com algum programa ou navegador as API.
	Os endere�os das API s�o as seguintes:
		> http://127.0.0.1/salesAPI/public/api/listSales  (Lista as vendas realizadas de acordo com o filtro passado - POST);
		> http://127.0.0.1/salesAPI/public/api/cashiers   (Lista todos caixas cadastrados para popular o Spinner do Android - POST);
		>PS. Os endere�os acima pode modificar se feito o deploy corretamente do projeto Laravel. Quando feito o deploy, o trecho "/public" n�o ser� mais necess�rio.
	