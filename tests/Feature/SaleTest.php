<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Http\Controllers\API\SaleController;
use Illuminate\Http\Request;

class SaleTest extends TestCase
{


    public function testRoute()
    {
        $response = $this->call('GET', '/');
        $response->assertStatus(200);
    }

    public function testSaleController()
    {
        $response = $this->call('GET', 'api/sales');
        $this->assertTrue($response->isOk());
    }
}
