<?php

use Faker\Generator as Faker;

$factory->define(App\Cashier::class, function (Faker $faker) {
    $name = [
        'Alpha-',
        'Beta-',
    ];

    return [
        'name' => $name[rand(0, count($name) - 1)].str_random(2),
    ];
});
