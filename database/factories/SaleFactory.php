<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Sale::class, function (Faker $faker) {
    $status = [
        'REJEITADA',
        'FATURADA',
        'CANCELADA',
        'PENDENTE',
    ];

    $values = [
        6.00,
        14.01,
        10.00,
        24.50,
    ];

    $dates = [
        '2018-05-20',
        '2018-05-21',
        '2018-05-22',
    ];

    $sales = App\Cashier::all();

    return [
        'receipt' => rand(100, 999),
        'sale_date' => $dates[rand(0, count($dates) - 1)],
        'cashier' => $sales[rand(0, count($sales) - 1)]->name,
        'status' => $status[rand(0, count($status) - 1)],
        'amount' => $values[rand(0, count($values) - 1)],
    ];
});
